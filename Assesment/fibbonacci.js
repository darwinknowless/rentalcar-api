// TODO : Number 02 Assesment
function fibonacci(num) {
	let arr = [];
	let first = 0;
	let second = 1;
	let sum;

	while (num > 0) {
		sum = first + second;
		arr.push(sum);

		first = second;
		second = sum;

		num = num - 1;
	}
	return arr;
}
console.log(fibonacci(8));
console.log(fibonacci(13));
