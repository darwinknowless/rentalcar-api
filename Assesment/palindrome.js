// TODO : Number 04 Assesment
function isPalindrome(line) {
	let q = line.split("").reverse().join("");
	return q == line;
}

console.log(isPalindrome("anna")); // => true
console.log(isPalindrome("water")); // => false
