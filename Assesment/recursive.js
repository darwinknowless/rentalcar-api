// TODO : Number 03 Assesment
// hitung a pangkat n
function pow(a, n) {
	if (a > 0 && n === 0) {
		return 1;
	}
	x = a;
	while (n > 1) {
		x = a * x;
		n--;
	}
	return x;
}

// hitung a pangkat n
console.log(pow(0, 0) + "\n"); // 0
console.log(pow(2, 4) + "\n"); // 16
console.log(pow(2, 0) + "\n"); // 1
console.log(pow(0, 2) + "\n"); // 0
