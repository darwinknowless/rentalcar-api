# **RENTAL CAR API**

## **About The Apps**

API for customer or client who need cars for vacation, work or anything activities.

## **How to Running this App**

1. Clone this repository:

```
$ git clone https://gitlab.com/darwinknowless/rentalcar-api.git
```

2. Create your own branch

```
$ git checkout -b <your>
```

3. NPM install (because this app using express)

```
$ npm i
```

4. Running App:

```
$ npm run server
```
