const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create model/schema/table
const CarSchema = new Schema(
	{
		name: {
			// nama mobil
			type: String,
			required: true,
		},
		number: {
			// nopol
			type: String,
			required: true,
			// unique: true,
		},
		type: {
			// jenis mobil
			type: String,
			required: true,
		},
		description: {
			// deskripsi
			type: String,
		},
		production: {
			//tahun pembuatan
			type: String,
			required: true,
		},
		yearofentry: {
			//tahun masuk
			type: Date,
			required: true,
		},
		status: {
			//tahun pembuatan
			type: String,
			required: true,
		},
		price: {
			//harga sewa
			type: Number,
			required: true,
		},
	},
	{
		timestamps: {
			createdAt: "created_at",
			updatedAt: "updated_at",
		},
		toJSON: { getters: true },
	}
);

// Exports model
module.exports = Car = mongoose.model("car", CarSchema);
