const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ClientSchema = new Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "user", // ref from user model
	},
	company: {
		type: String,
		required: true,
	},
	address: {
		type: String,
		required: true,
	},
	location: {
		type: String,
		required: true,
	},
	status: {
		type: String,
		required: true,
	},
	phone: {
		type: String,
		required: true,
	},
	date: {
		type: Date,
		default: Date.now,
	},
});

// Exports model
module.exports = Client = mongoose.model("client", ClientSchema);
