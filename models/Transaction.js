const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TransactionSchema = new Schema(
	{
		car_id: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "car",
		},
		client_id: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "client",
		},
		indate: {
			type: Date,
			required: true,
		},
		outdate: {
			type: Date,
			required: true,
		},
		note: {
			type: String,
		},
		rent: {
			// total day
			type: Number,
			required: true,
		},
		warranty: {
			// jaminan
			type: String,
			required: true,
		},
	},
	{
		timestamps: {
			createdAt: "created_at",
			updatedAt: "updated_at",
		},
		toJSON: { getters: true },
	}
);

// Exports model
module.exports = Transaction = mongoose.model("transaction", TransactionSchema);
