const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const auth = require("../../middlewares/auth");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");
// Import user model
const User = require("../../models/User");

// @route       GET api/auth
// @desc        Test route
// @access      public
router.get("/", auth, async (req, res) => {
	try {
		const user = await User.findById(req.user.id).select("-password");
		res.json(user);
	} catch (err) {
		console.log(err.message);
		res.status(500).send("Server Error");
	}
});

// @route       GET api/auth
// @desc        Authenticate user & get token
// @access      public
router.post(
	"/",
	[
		check("email", "Please include a valid email").isEmail(),
		check("password", "Password is required").exists(),
	],
	async (req, res) => {
		const errors = validationResult(req);

		if (!errors.isEmpty) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { email, password } = req.body;

		try {
			// See if user exists
			let user = await User.findOne({ email });
			if (!user) {
				res.status(400).json({
					errors: [{ msg: "Invalid Credentials" }],
				});
			}

			// Match password/compare
			const isMatch = await bcrypt.compare(password, user.password);
			if (!isMatch) {
				res.status(400).json({
					errors: [{ msg: "Invalid Credentials" }],
				});
			}

			// Return jsonwebtoken
			const payload = {
				user: {
					id: user.id,
				},
			};

			jwt.sign(
				payload,
				config.get("JWT_SECRET"),
				{ expiresIn: 360000 },
				(err, token) => {
					if (err) throw err;
					res.json({ token });
				}
			);
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server Error");
		}
	}
);

module.exports = router;
