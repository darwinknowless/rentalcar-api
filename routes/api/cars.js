const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const auth = require("../../middlewares/auth");

const Car = require("../../models/Car");

router.post("/", [auth], async (req, res) => {
	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		return res.status(400).json({ errors: errors.array() });
	}

	try {
		const newCar = new Car({
			name: req.body.name,
			number: req.body.number,
			type: req.body.type,
			description: req.body.description,
			production: req.body.production,
			yearofentry: req.body.yearofentry,
			status: req.body.status,
			price: req.body.price,
		});

		const car = await newCar.save();

		res.json(car);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server Error");
	}
});

router.get("/", [auth], async (req, res) => {
	try {
		const cars = await Car.find().sort();
		res.json(cars);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server Error");
	}
});

router.get("/:id", [auth], async (req, res) => {
	try {
		// Find
		const car = await Car.findById(req.params.id);
		// If not found
		if (!car) {
			return res.status(404).json({ msg: "Car not found" });
		}
		// Result/return
		res.json(car);
	} catch (err) {
		console.error(err.message);
		// If not object ID
		if (err.kind == "ObjectId") {
			return res.status(400).json({ msg: "Car not found" });
		}
		res.status(500).send("Server Error");
	}
});

router.delete("/:id", [auth], async (req, res) => {
	try {
		// Find
		const car = await Car.findById(req.params.id);
		// If not found
		if (!car) {
			return res.status(404).json({ msg: "Car not found" });
		}
		// Delete
		await car.remove();
		// Result/return
		res.json({ msg: "Post removed" });
	} catch (err) {
		console.error(err.message);
		// If not object ID
		if (err.kind == "ObjectId") {
			return res.status(400).json({ msg: "Car not found" });
		}
		res.status(500).send("Server Error");
	}
});

module.exports = router;
