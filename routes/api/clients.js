const express = require("express");
const request = require("request");
const config = require("config");
const router = express.Router();
const auth = require("../../middlewares/auth");
const { check, validationResult } = require("express-validator");
// Import model
const Client = require("../../models/Client");
const User = require("../../models/User");

// @route       GET api/client/me
// @desc        Get current users client
// @access      private
router.get("/me", auth, async (req, res) => {
	try {
		// Find client user
		const client = await Client.findOne({ user: req.user.id }).populate(
			"user",
			["name", "avatar"]
		);
		// If no client
		if (!client) {
			return res
				.status(400)
				.json({ msg: "There is no client for this user" });
		}
		// If success
		res.json(client);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server Error");
	}
});

// @route       POST api/client
// @desc        Create or update user client
// @access      private
router.post(
	"/",
	[
		auth,
		[
			check("company", "Company is required").not().isEmpty(),
			check("address", "Address is required").not().isEmpty(),
			check("location", "Location is required").not().isEmpty(),
			check("status", "Status is required").not().isEmpty(),
			check("phone", "Phone is required").not().isEmpty(),
		],
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Fields
		const { company, address, location, status, phone } = req.body;

		// Build client object
		const clientFields = {};
		clientFields.user = req.user.id;
		if (company) clientFields.company = company;
		if (address) clientFields.address = address;
		if (location) clientFields.location = location;
		if (status) clientFields.status = status;
		if (phone) clientFields.phone = phone;

		try {
			// Find user
			let client = await Client.findOne({ user: req.user.id });
			// If client found, update
			if (client) {
				// Update
				client = await client.findOneAndUpdate(
					{ user: req.user.id },
					{ $set: clientFields },
					{ new: true }
				);
				// Result/return
				return res.json(client);
			}
			// If no client found, create new
			// Create
			client = new Profile(clientFields);
			await client.save();
			// Result/return
			res.json(client);
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server Error");
		}
	}
);

// @route       GET api/client
// @desc        Get all clients
// @access      public
router.get("/", async (req, res) => {
	try {
		// Find all user & populate
		let clients = await Client.find().populate("user", ["name", "avatar"]);
		// Result/return
		res.json(clients);
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server Error");
	}
});

// @route       GET api/client/user/:user_id
// @desc        Get client by user ID
// @access      public
router.get("/user/:user_id", async (req, res) => {
	try {
		// Find user by params ID & populate
		let client = await Client.findOne({
			user: req.params.user_id,
		}).populate("user", ["name", "avatar"]);
		// If not found
		if (!client) return res.status(400).json({ msg: "Client not found" });
		// Result/return
		res.json(client);
	} catch (err) {
		console.error(err.message);
		// If not object ID
		if (err.kind == "ObjectId") {
			return res.status(400).json({ msg: "Client not found" });
		}
		res.status(500).send("Server Error");
	}
});

// @route       DELETE api/client
// @desc        Delete client, user & other
// @access      private
router.delete("/", auth, async (req, res) => {
	try {
		// Remove client
		await Client.findOneAndRemove({ user: req.user.id });
		// Remove user
		await User.findOneAndRemove({ _id: req.user.id });
		// Result/return
		res.json({ msg: "User deleted" });
	} catch (err) {
		console.error(err.message);
		res.status(500).send("Server Error");
	}
});

module.exports = router;
