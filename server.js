const express = require("express");
const connectDB = require("./config/database");

const app = express();

//Connect database
connectDB();

//Init middleware
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("API Running"));

// ============== ROUTES DECLARATION & IMPORT ====================== //
const authRoute = require("./routes/api/auth");
app.use("/api/auth", authRoute);

const clientRoute = require("./routes/api/clients");
app.use("/api/clients", clientRoute);

const userRoute = require("./routes/api/users");
app.use("/api/users", userRoute);

const carRoute = require("./routes/api/cars");
app.use("/api/cars", carRoute);
// ============== END ROUTES DECLARATION & IMPORT ====================== //
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
